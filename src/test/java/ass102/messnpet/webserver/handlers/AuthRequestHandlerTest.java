package ass102.messnpet.webserver.handlers;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.response.IResponseWriter;
import ass102.messnpet.webserver.server.IWebRequestHandler;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test třídy AuthRequestHandler - handleru poskytující základní autorizaci
 *
 * @author Petr Messner
 */
public class AuthRequestHandlerTest {

    public AuthRequestHandlerTest() {
    }

    @Test
    public void testProcessRequestWithoutAuthentication() throws IOException {
        IWebRequestHandler helloHandler = new HelloWorldRequestHandler();
        AuthRequestHandler authHandler = new AuthRequestHandler(helloHandler, "Secure Area");
        authHandler.add("john", "aaa");
        final Request req = new Request("GET", "/", null, null);
        IResponseWriter response = authHandler.processRequest(req);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        response.writeResponseToSocketOutputStream(bos);
        assertEquals(
                "HTTP/1.0 401 Authorization Required\r\n"
                + "WWW-Authenticate: Basic realm=\"Secure Area\"\r\n\r\n",
                bos.toString("utf-8"));
    }

    @Test
    public void testProcessRequestWithInvalidAuthentication() throws IOException {
        IWebRequestHandler helloHandler = new HelloWorldRequestHandler();

        AuthRequestHandler authHandler = new AuthRequestHandler(helloHandler, "Secure Area");
        authHandler.add("john", "aaa");

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic am9objpiYmI=");
        final Request req = new Request("GET", "/", null, headers);

        IResponseWriter response = authHandler.processRequest(req);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        response.writeResponseToSocketOutputStream(bos);
        assertEquals(
                "HTTP/1.0 401 Authorization Required\r\n"
                + "WWW-Authenticate: Basic realm=\"Secure Area\"\r\n\r\n",
                bos.toString("utf-8"));
    }

    @Test
    public void testProcessRequestWithCorrectAuthentication() throws IOException {
        IWebRequestHandler helloHandler = new HelloWorldRequestHandler();

        AuthRequestHandler authHandler = new AuthRequestHandler(helloHandler, "Secure Area");
        authHandler.add("john", "aaa");

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic am9objphYWE=");
        final Request req = new Request("GET", "/", null, headers);

        IResponseWriter response = authHandler.processRequest(req);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        response.writeResponseToSocketOutputStream(bos);
        assertEquals(
                "HTTP/1.0 200 OK\r\n\r\n"
                + "Hello, World!",
                bos.toString("utf-8"));
    }
}
