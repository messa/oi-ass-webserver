package ass102.messnpet.webserver.handlers;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.response.IResponseWriter;
import ass102.messnpet.webserver.response.OKResponse;
import ass102.messnpet.webserver.server.IWebRequestHandler;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test třídy CachingWebRequstHandler - request handleru, který kešuje odpovědi
 *
 * @author Petr Messner
 */
public class CachingWebRequestHandlerTest {

    public CachingWebRequestHandlerTest() {
    }

    /**
     * Otestuje se nastavení a získání hodnoty maxCachedResponseSizeBytes.
     *
     * Konkrétně se otestují metody setMaxCachedResponseSizeBytes a
     * getMaxCachedResponseSizeBytes.
     */
    @Test
    public void testSetAndGetMaxCachedResponseSizeBytes() {
        CachingWebRequestHandler crh = new CachingWebRequestHandler(null);
        crh.setMaxCachedResponseSizeBytes(123);
        assertEquals(123, crh.getMaxCachedResponseSizeBytes());
    }

    /**
     * Otestování, zda handler skutečně kešuje.
     *
     * Tedy že návratová hodnota metody processRequest se vrátí z cache,
     * pokud je stejný požadavek (argument processRequest) zavolaný podruhé.
     */
    @Test
    public void testProcessRequestIsReallyCaching() throws IOException {
        IWebRequestHandler countingHandler = new IWebRequestHandler() {

            private int counter = 0;

            public IResponseWriter processRequest(Request r) {
                this.counter++;
                return new OKResponse().setBody(Integer.toString(this.counter));
            }
        };

        IWebRequestHandler cachingHandler = new CachingWebRequestHandler(countingHandler);

        final Request req = new Request("GET", "/", null, null);

        {
            // zavolá se countingHandler přímo, vrátí 1
            IResponseWriter response = countingHandler.processRequest(req);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            response.writeResponseToSocketOutputStream(bos);
            assertEquals("HTTP/1.0 200 OK\r\n\r\n1", bos.toString("utf-8"));
        }

        {
            // zavolá se countingHandler přímo, vrátí 2 - ověření, že
            // countingHandler funguje
            IResponseWriter response = countingHandler.processRequest(req);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            response.writeResponseToSocketOutputStream(bos);
            assertEquals("HTTP/1.0 200 OK\r\n\r\n2", bos.toString("utf-8"));
        }

        {
            // zavolá se countingHandler přes cachingHandler, vrátí se
            // 3 a tato hodnota by se měla nakešovat
            IResponseWriter response = cachingHandler.processRequest(req);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            response.writeResponseToSocketOutputStream(bos);
            assertEquals("HTTP/1.0 200 OK\r\n\r\n3", bos.toString("utf-8"));
        }

        {
            // zavolá se opět countingHandler přes cachingHandler, tím se
            // ověří, že se vrátí nakešovaná hodnota 3 a ne 4, kterou by
            // vrátil sám countingHandler
            IResponseWriter response = cachingHandler.processRequest(req);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            response.writeResponseToSocketOutputStream(bos);
            assertEquals("HTTP/1.0 200 OK\r\n\r\n3", bos.toString("utf-8"));
        }

    }
}
