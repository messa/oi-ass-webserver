package ass102.messnpet.webserver.handlers;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.response.IResponseWriter;
import ass102.messnpet.webserver.server.IWebRequestHandler;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Messner
 */
public class HelloWorldRequestHandlerTest {

    public HelloWorldRequestHandlerTest() {
    }

    /**
     * Test of processRequest method, of class HelloWorldRequestHandler.
     */
    @Test
    public void testProcessRequest() throws IOException {
        IWebRequestHandler helloHandler = new HelloWorldRequestHandler();

        final Request req = new Request("GET", "/", null, null);

        IResponseWriter response = helloHandler.processRequest(req);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        response.writeResponseToSocketOutputStream(bos);
        assertEquals(
                "HTTP/1.0 200 OK\r\n\r\n"
                + "Hello, World!",
                bos.toString("utf-8"));
    }
}
