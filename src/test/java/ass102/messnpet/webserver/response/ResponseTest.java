package ass102.messnpet.webserver.response;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Petr Messner
 */
public class ResponseTest {

    public ResponseTest() {
    }

    public class ResponseImpl extends Response {

        public int getStatusCode() {
            return 200;
        }

        public String getStatusMessage() {
            return "OK";
        }
    }

    @Test
    public void testEmptyResponseCanBeWritten() throws IOException {
        Response r = new ResponseImpl();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        r.writeResponseToSocketOutputStream(out);
        String s = out.toString("utf-8");
        assertEquals("HTTP/1.0 200 OK\r\n\r\n", s);

    }

    @Test(expected = AssertionError.class)
    public void testCannotBeWrittenMoreThanOnce() throws IOException {
        Response r = new ResponseImpl();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        r.writeResponseToSocketOutputStream(out);
        r.writeResponseToSocketOutputStream(out);
    }

    /**
     * Test of setBody method, of class Response.
     */
    @Test
    public void testSetBody_String() throws IOException {
        Response r = new ResponseImpl();
        r.setBody("foo");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        r.writeResponseToSocketOutputStream(out);
        String s = out.toString("utf-8");
        assertEquals("HTTP/1.0 200 OK\r\n\r\nfoo", s);

    }

    /**
     * Test of setBody method, of class Response.
     */
    @Test
    public void testSetBody_byteArr() throws IOException {
        Response r = new ResponseImpl();
        byte[] ba = new byte[3];
        ba[0] = 'f';
        ba[1] = 'o';
        ba[2] = 'o';
        r.setBody(ba);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        r.writeResponseToSocketOutputStream(out);
        String s = out.toString("utf-8");
        assertEquals("HTTP/1.0 200 OK\r\n\r\nfoo", s);
    }

    /**
     * Test of setBody method, of class Response.
     */
    @Test
    public void testSetBody_InputStream() throws IOException {
        Response r = new ResponseImpl();
        byte[] ba = new byte[3];
        ba[0] = 'f';
        ba[1] = 'o';
        ba[2] = 'o';
        r.setBody(new ByteArrayInputStream(ba));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        r.writeResponseToSocketOutputStream(out);
        String s = out.toString("utf-8");
        assertEquals("HTTP/1.0 200 OK\r\n\r\nfoo", s);
    }
}
