package ass102.messnpet.webserver.response;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * @author Petr Messner
 */
public class ResponseFactoryTest {

    public ResponseFactoryTest() {
    }

    /**
     * Test of createOKResponse method, of class ResponseFactory.
     */
    @Test
    public void testCreateOKResponse() throws IOException {
        Response r = ResponseFactory.createOKResponse();

        assertEquals(200, r.getStatusCode());
        assertEquals("OK", r.getStatusMessage());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        r.writeResponseToSocketOutputStream(bos);
        assertEquals(
                "HTTP/1.0 200 OK\r\n\r\n",
                bos.toString("utf-8"));
    }

    /**
     * Test of createNotFoundResponse method, of class ResponseFactory.
     */
    @Test
    public void testCreateNotFoundResponse() throws IOException {
        Response r = ResponseFactory.createNotFoundResponse();

        assertEquals(404, r.getStatusCode());
        assertEquals("Not Found", r.getStatusMessage());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        r.writeResponseToSocketOutputStream(bos);
        assertEquals(
                "HTTP/1.0 404 Not Found\r\n\r\n",
                bos.toString("utf-8"));
    }

    /**
     * Test of createServerErrorResponse method, of class ResponseFactory.
     */
    @Test
    public void testCreateServerErrorResponse() throws IOException {
        Response r = ResponseFactory.createServerErrorResponse();

        assertEquals(500, r.getStatusCode());
        assertEquals("Server Error", r.getStatusMessage());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        r.writeResponseToSocketOutputStream(bos);
        assertEquals(
                "HTTP/1.0 500 Server Error\r\n\r\n",
                bos.toString("utf-8"));
    }

    /**
     * Test of createAuthorizationRequiredResponse method, of class
     * ResponseFactory.
     */
    @Test
    public void testCreateAuthorizationRequiredResponse() throws IOException {
        Response r = ResponseFactory.createAuthorizationRequiredResponse("some realm");

        assertEquals(401, r.getStatusCode());
        assertEquals("Authorization Required", r.getStatusMessage());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        r.writeResponseToSocketOutputStream(bos);
        assertEquals(
                "HTTP/1.0 401 Authorization Required\r\n"
                + "WWW-Authenticate: Basic realm=\"some realm\"\r\n\r\n",
                bos.toString("utf-8"));
    }
}
