package ass102.messnpet.webserver.server;

import ass102.messnpet.webserver.handlers.HelloWorldRequestHandler;
import ass102.messnpet.webserver.util.TCPUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Petr Messner
 */
public class HTTPServerTest {

    static final Logger log = Logger.getLogger(HTTPServerTest.class.getName());
    int httpServerPort;
    IServer httpServer;

    public HTTPServerTest() {
    }

    @Before
    public void setUp() throws InterruptedException {
        this.httpServerPort = TCPUtils.getUnunsedPort();
        IWebRequestHandler requesthandler = new HelloWorldRequestHandler();
        this.httpServer = HTTPServerFactory.startHTTPServerInThread(httpServerPort, requesthandler);
    }

    @After
    public void tearDown() {
        this.httpServer.stop();
    }

    @Test
    public void testSmoke() {
    }

    @Test
    public void testSimple() throws IOException {
        Socket s = new Socket("127.0.0.1", this.httpServerPort);
        OutputStreamWriter w = new OutputStreamWriter(s.getOutputStream());
        w.write("GET / HTTP/1.0\r\n");
        w.write("Host: 127.0.0.1\r\n");
        w.write("\r\n");
        w.flush();

        BufferedReader r = new BufferedReader(new InputStreamReader(s.getInputStream()));
        String line;
        line = r.readLine();
        assertEquals("HTTP/1.0 200 OK", line);
        line = r.readLine();
        assertEquals("", line);
        line = r.readLine();
        assertEquals("Hello, World!", line);

        s.close();
    }
}
