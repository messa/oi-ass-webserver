package ass102.messnpet.webserver.server;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.response.OKResponse;
import ass102.messnpet.webserver.response.Response;
import ass102.messnpet.webserver.server.HTTPServerFactory;
import ass102.messnpet.webserver.server.IServer;
import ass102.messnpet.webserver.server.IWebRequestHandler;
import ass102.messnpet.webserver.util.TCPUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Petr Messner
 */
public class ThreadingHTTPServerTest {

    private static final Logger logger = Logger.getLogger(ThreadingHTTPServerTest.class.getName());
    private int httpServerPort;
    private IServer httpServer = null;
    private static long TIMEOUT_MILIS = 1000;

    public ThreadingHTTPServerTest() {
    }

    @Before
    public void setUp() {
        this.httpServerPort = TCPUtils.getUnunsedPort();
    }

    @After
    public void tearDown() {
        logger.info("teardown...");
        if (this.httpServer != null) {
            this.httpServer.stop();
        }
    }

    @Test
    public void testSmoke() {
        logger.info("smoke");
    }

    @Test
    public void testOneClient() throws InterruptedException {
        logger.info("testOneClient start");
        parallelClients(1);
    }

    @Test
    public void testFiveParallelClients() throws InterruptedException {
        logger.info("test five parallel clients start");
        parallelClients(5);
    }

    private void parallelClients(int n) throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(n);
        final Object lock = new Object();

        logger.info("preparing webserver...");

        IWebRequestHandler requestHandler = new IWebRequestHandler() {

            @Override
            public Response processRequest(Request request) {
                try {
                    synchronized (lock) {
                        latch.countDown();
                        lock.wait();
                    }
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
                return new OKResponse().setBody("Hello, World!");
            }
        };

        assert this.httpServer == null;
        this.httpServer = HTTPServerFactory.startThreadingHTTPServerInThread(this.httpServerPort, requestHandler);

        //logger.info("starting client threads...");

        List<Thread> clientThreads = new ArrayList<Thread>();
        for (int i = 0; i < n; i++) {
            final Thread t = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Socket s = new Socket("127.0.0.1", httpServerPort);
                        OutputStreamWriter w = new OutputStreamWriter(s.getOutputStream());
                        w.write("GET / HTTP/1.0\r\n");
                        w.write("Host: 127.0.0.1\r\n");
                        w.write("\r\n");
                        w.flush();
                        BufferedReader r = new BufferedReader(new InputStreamReader(s.getInputStream()));
                        String line;
                        line = r.readLine();
                        assertEquals("HTTP/1.0 200 OK", line);
                        line = r.readLine();
                        assertEquals("", line);
                        line = r.readLine();
                        assertEquals("Hello, World!", line);
                        s.close();
                    } catch (IOException ex) {
                        logger.log(Level.SEVERE,
                                "Caught IOException " + ex
                                + " in client thread " + Thread.currentThread(),
                                ex);
                        throw new RuntimeException(ex);
                    }
                }
            });
            t.start();
            clientThreads.add(t);
        }

        //logger.info("checking latch is zero...");

        boolean latchReachedZero = latch.await(TIMEOUT_MILIS, TimeUnit.MILLISECONDS);
        assertEquals("latch.await() has not timeouted", true, latchReachedZero);
        assertEquals("expected countDown call count", 0, latch.getCount());

        synchronized (lock) {
            lock.notifyAll();
        }

        logger.info("stopping client threads...");

        for (Thread t : clientThreads) {
            t.join(TIMEOUT_MILIS);
            assertEquals("client thread terminated", false, t.isAlive());
        }
    }
}
