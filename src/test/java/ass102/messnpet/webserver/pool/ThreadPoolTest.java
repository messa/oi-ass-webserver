package ass102.messnpet.webserver.pool;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test třídy ThreadPool.
 *
 * @author Petr Messner
 */
public class ThreadPoolTest {

    public ThreadPoolTest() {
    }

    @Test
    public void testOne() throws InterruptedException {
        System.err.println("testOne start");
        ThreadPool pool = new ThreadPool().setMaxWorkerCount(5);
        pool.start();

        final Semaphore s = new Semaphore(0);

        Runnable r = new Runnable() {

            @Override
            public void run() {
                try {
                    s.acquire();
                } catch (InterruptedException ex) {
                    Logger.getLogger(ThreadPoolTest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        pool.process(r);

        System.err.println("Sleeping...");
        Thread.sleep(1000);
        System.err.println("Sleep done");
        assertEquals(1, s.getQueueLength());
        s.release(1);
        System.err.println("xxx");
        pool.stop();
        System.err.println("testOne end");
    }

    @Test
    public void testFive() throws InterruptedException {
        System.err.println("testFive start");
        ThreadPool pool = new ThreadPool().setMaxWorkerCount(5);
        pool.start();

        final Semaphore s = new Semaphore(0);

        for (int i = 0; i < 5; i++) {
            Runnable r = new Runnable() {

                @Override
                public void run() {
                    try {
                        s.acquire();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ThreadPoolTest.class.getName()).log(Level.SEVERE, "Caught InterruptedException while s.acquire", ex);
                    }
                }
            };
            pool.process(r);
        }

        System.err.println("Sleeping...");
        Thread.sleep(1000); // TODO XXX
        System.err.println("Sleep done");
        assertEquals(5, s.getQueueLength());
        s.release(5);
        Thread.sleep(1000); // TODO XXX
        pool.stop();
        System.err.println("testFive end");
    }
}
