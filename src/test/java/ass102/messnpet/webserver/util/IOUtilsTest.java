package ass102.messnpet.webserver.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test IOUtils
 *
 * @author Petr Messner
 */
public class IOUtilsTest {

    public IOUtilsTest() {
    }

    /**
     * Test of copy method, of class IOUtils.
     */
    @Test
    public void testCopyEmpty() throws Exception {
        ByteArrayInputStream in = new ByteArrayInputStream(new byte[0]);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(in, out);
        assertEquals(out.size(), 0);
    }

    @Test
    public void testCopyOneByte() throws IOException {
        byte[] a = new byte[1];
        a[0] = 'x';
        ByteArrayInputStream in = new ByteArrayInputStream(a);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(in, out);
        assertEquals(out.size(), 1);
        assertEquals(out.toByteArray()[0], 'x');
    }

    @Test
    public void testCopyMoreBytesThanBufferSize() throws IOException {
        byte[] a = new byte[3];
        a[0] = 'a';
        a[1] = 'b';
        a[2] = 'c';
        ByteArrayInputStream in = new ByteArrayInputStream(a);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(in, out, 2);
        assertEquals(out.size(), 3);
        assertEquals(out.toByteArray()[0], 'a');
        assertEquals(out.toByteArray()[1], 'b');
        assertEquals(out.toByteArray()[2], 'c');
    }
}
