/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ass102.messnpet.webserver.util;

import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author messa
 */
public class Base64UtilsTest {

    public Base64UtilsTest() {
    }

    /**
     * Test of decode method, of class Base64Utils.
     */
    @Test
    public void testDecode() {
        assertEquals("foo", Base64Utils.decode("Zm9v"));
    }
}
