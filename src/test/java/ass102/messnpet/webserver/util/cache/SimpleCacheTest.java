package ass102.messnpet.webserver.util.cache;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test SimpleCahce.
 *
 * @author Petr Messner
 */
public class SimpleCacheTest {

    public SimpleCacheTest() {
    }

    /**
     * Test of setMaxItemCount method, of class SimpleCache.
     */
    @Test
    public void testSetAndGetMaxItemCount() {
        SimpleCache<String, String> c = new SimpleCache<String, String>();
        c.setMaxItemCount(123);
        assertEquals(123, c.getMaxItemCount());
    }

    /**
     * Test of setCleanItemCount method, of class SimpleCache.
     */
    @Test
    public void testSetAndGetCleanItemCount() {
        SimpleCache<String, String> c = new SimpleCache<String, String>();
        c.setCleanItemCount(123);
        assertEquals(123, c.getCleanItemCount());
    }

    /**
     * Test of get method, of class SimpleCache.
     */
    @Test
    public void testGetFromEmptyReturnsNull() {
        SimpleCache<String, String> c = new SimpleCache<String, String>();
        assertNull(c.get("some key"));
    }

    /**
     * Test of set method, of class SimpleCache.
     */
    @Test
    public void testSetAndGet() {
        SimpleCache<String, String> c = new SimpleCache<String, String>();
        c.set("some key", "some value");
        assertEquals("some value", c.get("some key"));
        assertNull(c.get("some other key"));
    }

    /**
     * Test of clean method, of class SimpleCache.
     */
    @Test
    public void testClean() {
        SimpleCache<String, String> c = new SimpleCache<String, String>();
        c.setMaxItemCount(1);
        c.setCleanItemCount(2);
        c.set("key 1", "value 1");
        c.set("key 2", "value 2");
        assertEquals("value 1", c.get("key 1"));
        assertEquals("value 2", c.get("key 2"));
        c.clean();
        assertNull(c.get("key 1"));
        assertEquals("value 2", c.get("key 2"));
    }

    /**
     * Otestovani toho assertu v clean().
     */
    @Test(expected = AssertionError.class)
    public void testCleanItemCountMustBeEqualOrGreaterThanMaxItemCount() {
        SimpleCache<String, String> c = new SimpleCache<String, String>();
        c.setMaxItemCount(2);
        c.setCleanItemCount(1);
        c.set("some key", "some value");
    }
}
