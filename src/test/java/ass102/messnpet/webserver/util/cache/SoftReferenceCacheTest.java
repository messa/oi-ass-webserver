package ass102.messnpet.webserver.util.cache;

import java.lang.ref.SoftReference;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Testy SoftReferenceCache.
 *
 * @author Petr Messner
 */
public class SoftReferenceCacheTest {

    public SoftReferenceCacheTest() {
    }

    /**
     * Jenom takový sanity check, že samotná SoftReference funguje tak, jak
     * předpokládáme pro tento test.
     */
    @Test
    public void testSoftReferenceClearsAsExpected() {
        String s = "foobar";
        SoftReference<String> sr = new SoftReference<String>(s);
        assertTrue(sr.get() != null);
        sr.clear();
        assertTrue(sr.get() == null);
    }

    /**
     * Prázdná cache funguje - get() vrací null.
     */
    @Test
    public void testGetFromEmptyCacheReturnsNull() {
        SoftReferenceCache<String, String> cache = new SoftReferenceCache<String, String>();
        assertEquals(null, cache.get("foo"));
    }

    /**
     * Get vrátí to, co bylo nasetováno.
     */
    @Test
    public void testSetAndGet() {
        SoftReferenceCache<String, String> cache = new SoftReferenceCache<String, String>();
        String s = "foo value";
        cache.set("foo", s);
        assertEquals("foo value", cache.get("foo"));
    }

    /**
     * Get vrátí null, když byla softreference vymazána.
     */
    @Test
    public void testGetReturnsNullAfterReferenceIsCleared() {
        SoftReferenceCache<String, String> cache = new SoftReferenceCache<String, String>();
        String s = "foo value";
        SoftReference<String> sr = new SoftReference<String>(s);
        cache.set("foo", sr);
        assertEquals("foo value", cache.get("foo"));
        sr.clear();
        assertEquals(null, cache.get("foo"));
    }

    /**
     * Jenom spuštění a zastavení cleanup vlákna.
     */
    @Test
    public void testRunAndStopCleanupThread() throws InterruptedException {
        SoftReferenceCache<String, String> cache = new SoftReferenceCache<String, String>();
        cache.runCleanupThread();
        cache.stopCleanupThread();
    }

    /**
     * Test of cleanup method, of class SoftReferenceCache.
     */
    @Test
    public void testCleanupReallyCleansUp() {
        SoftReferenceCache<String, String> cache = new SoftReferenceCache<String, String>();
        String s = "foo value";
        SoftReference<String> sr = new SoftReference<String>(s);
        cache.set("foo", sr);
        assertEquals(1, cache.getMapSize());
        cache.cleanup();
        assertEquals(1, cache.getMapSize());
        sr.clear();
        cache.cleanup();
        assertEquals(0, cache.getMapSize());
    }
}
