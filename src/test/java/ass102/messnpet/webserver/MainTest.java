package ass102.messnpet.webserver;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Messner
 */
public class MainTest {

    public MainTest() {
    }

    /**
     * Test of parseArgs method, of class Main.
     */
    @Test
    public void testParseArgsEmpty() {
        String args[] = {};
        Main.Configuration conf = new Main.Configuration();
        assertEquals(8000, conf.getPort());
        assertEquals(Main.Configuration.Action.SERVE, conf.getAction());
        Main.parseArgs(args, conf);
        assertEquals(8000, conf.getPort());
        assertEquals(Main.Configuration.Action.SERVE, conf.getAction());
    }

    /**
     * Test of parseArgs method, of class Main.
     */
    @Test
    public void testParseArgsRunBenchmark() {
        String args[] = {"--benchmark"};
        Main.Configuration conf = new Main.Configuration();
        assertEquals(Main.Configuration.Action.SERVE, conf.getAction());
        Main.parseArgs(args, conf);
        assertEquals(Main.Configuration.Action.RUN_BENCHMARK, conf.getAction());
    }

    /**
     * Test of parseArgs method, of class Main.
     */
    @Test
    public void testParseArgsSetPort() {
        String args[] = {"--port", "4000"};
        Main.Configuration conf = new Main.Configuration();
        assertEquals(8000, conf.getPort());
        Main.parseArgs(args, conf);
        assertEquals(4000, conf.getPort());
    }
}
