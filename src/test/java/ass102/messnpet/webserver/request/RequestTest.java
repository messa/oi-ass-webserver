package ass102.messnpet.webserver.request;

import java.util.Map;
import java.util.TreeMap;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Messner
 */
public class RequestTest {

    public RequestTest() {
    }

    /**
     * Test of getMethod method, of class Request.
     */
    @Test
    public void testGetMethod() {
        Request r1 = new Request("GET", null, null, null);
        assertEquals("GET", r1.getMethod());
        Request r2 = new Request("POST", null, null, null);
        assertEquals("POST", r2.getMethod());
    }

    /**
     * Test of getPath method, of class Request.
     */
    @Test
    public void testGetPath() {
        Request r = new Request(null, "/somePath", null, null);
        assertEquals("/somePath", r.getPath());
    }

    /**
     * Test of getProtocol method, of class Request.
     */
    @Test
    public void testGetProtocol() {
        Request r = new Request(null, null, "HTTP/1.1", null);
        assertEquals("HTTP/1.1", r.getProtocol());
    }

    /**
     * Test of getHeaders method, of class Request.
     */
    @Test
    public void testGetHeaders() {
        Request r;
        {
            Map<String, String> headers = new TreeMap<String, String>();
            headers.put("host", "www.example.com");
            r = new Request(null, null, null, headers);
        }
        {
            Map<String, String> headers = r.getHeaders();
            assertEquals(1, headers.size());
            assertEquals("www.example.com", headers.get("host"));
        }
    }

    @Test(expected = java.lang.UnsupportedOperationException.class)
    public void testGetHeadersResultIsUnmodifiable() {
        Request r;
        {
            Map<String, String> headers = new TreeMap<String, String>();
            r = new Request(null, null, null, headers);
        }
        {
            Map<String, String> headers = r.getHeaders();
            headers.put("foo", "bar");
        }
    }

    /**
     * Test of getHeader method, of class Request.
     */
    @Test
    public void testGetHeader() {
        Request r;
        {
            Map<String, String> headers = new TreeMap<String, String>();
            headers.put("host", "www.example.com");
            r = new Request(null, null, null, headers);
        }
        assertEquals("www.example.com", r.getHeader("Host"));
        assertEquals("www.example.com", r.getHeader("host"));
        assertEquals("www.example.com", r.getHeader("HOST"));
    }

    /**
     * Test of toString method, of class Request.
     */
    @Test
    public void testToString() {
        {
            Request r = new Request(null, null, null, null);
            assertEquals("<Request: null null>", r.toString());
        }
        {
            Request r = new Request("GET", "/foo", "HTTP/1.1", new TreeMap<String, String>());
            assertEquals("<Request: GET /foo>", r.toString());
        }
    }
}
