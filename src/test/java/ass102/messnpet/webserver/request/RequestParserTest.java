package ass102.messnpet.webserver.request;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Otestování třídy RequestParser
 *
 * @author Petr Messner
 */
public class RequestParserTest {

    public RequestParserTest() {
    }

    /**
     * Test parsování jednoduchého HTTP požadavku.
     */
    @Test
    public void testParseRequest() {
        String[] lines = {
            "GET / HTTP/1.1",
            "Content-Length: 1234"
        };

        Request r = RequestParser.parseRequest(lines);

        assertEquals("GET", r.getMethod());
        assertEquals("/", r.getPath());
        assertEquals("HTTP/1.1", r.getProtocol());
        assertEquals("1234", r.getHeader("Content-Length"));
        assertEquals("1234", r.getHeader("content-length"));
        assertEquals("<Request: GET />", r.toString());
    }
}
