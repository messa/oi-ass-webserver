package ass102.messnpet.webserver.server;

/**
 * Rozhraní pro ovládání serveru
 *
 * @author Petr Messner
 */
public interface IServer {

    /**
     * Spuštění
     */
    public void start();

    /**
     * Zastavení
     */
    public void stop();
}
