package ass102.messnpet.webserver.server;

import java.net.Socket;

/**
 * Rozhraní pro objekty, které umí obsloužit připojeného klienta.
 *
 * Jedná se o nízkoúrovňový přístup, zpracovává se síťový socket získaný
 * acceptem.
 *
 * @author Petr Messner
 */
public interface ISocketClientHandler {

    void processRequest(Socket s);
}
