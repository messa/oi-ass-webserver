package ass102.messnpet.webserver.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TCP server
 *
 * Poslouchá na daném TCP portu, každé přijaté spojení je předané
 * socketClientHandleru, který je serveru předán konstruktorem.
 *
 * @author Petr Messner
 */
public class TCPServer implements IServer {

    private static final Logger logger = Logger.getLogger(TCPServer.class.getName());
    private static int backlog = 100;
    private int port;
    private boolean shouldStop = false;
    private ServerSocket listeningSocket = null;
    private ISocketClientHandler socketClientHandler;
    private final CountDownLatch startedLatch = new CountDownLatch(1);

    /**
     *
     * @param port Číslo TCP portu, na kterém server bude poslouchat
     * @param socketClientHandler Handler, kterému bude předané každé přijaté
     * spojení
     */
    public TCPServer(int port, ISocketClientHandler socketClientHandler) {
        assert port >= 0;
        this.port = port;
        this.socketClientHandler = socketClientHandler;
    }

    public void waitUntilStarted() throws InterruptedException {
        this.startedLatch.await();
    }

    @Override
    public void start() {
        assert listeningSocket == null;
        try {
            listeningSocket = new ServerSocket(this.port, TCPServer.backlog);

            logger.log(Level.INFO, "Listening at {0}...", listeningSocket.getLocalSocketAddress());

            this.startedLatch.countDown();

            for (;;) {
                Socket clientSocket = this.listeningSocket.accept();

                boolean stop;
                synchronized (this) {
                    stop = this.shouldStop;
                }
                if (stop) {
                    clientSocket.close();
                    break;
                }

                logger.log(Level.INFO, "Accepted client {0}", clientSocket);

                this.socketClientHandler.processRequest(clientSocket);
            }

            this.listeningSocket.close();

        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void stop() {
        synchronized (this) {
            this.shouldStop = true;
        }
        logger.log(Level.INFO, "Stopping ({0})", this.listeningSocket);
        try {
            Socket s = new Socket(
                    this.listeningSocket.getInetAddress(),
                    this.listeningSocket.getLocalPort());
            s.close();
        } catch (IOException ex) {
            throw new RuntimeException("Could not issue a stop request", ex);
        }

    }
}
