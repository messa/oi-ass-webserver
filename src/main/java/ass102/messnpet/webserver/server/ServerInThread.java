package ass102.messnpet.webserver.server;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Spuštění serveru ve vlákně
 *
 * Vhodné např. pro testování, aby vlákno, které chce spustit server, mohlo
 * dále pokračovat v činnosti.
 *
 * @author Petr Messner
 */
public class ServerInThread implements IServer {

    private static final Logger logger = Logger.getLogger(ServerInThread.class.getName());

    IServer server;
    Thread thread = null;

    /**
     * @param server Server, který bude spuštěn ve vlákně
     */
    public ServerInThread(IServer server) {
        this.server = server;
    }

    /**
     * Spuštění
     */
    @Override
    public void start() {
        assert this.thread == null;
        this.thread = new Thread(new StartServer(this.server), "server");
        this.thread.start();
    }

    /**
     * Zastavení (ukončení)
     */
    @Override
    public void stop() {
        assert this.thread != null;
        this.server.stop();
        try {
            this.thread.join();
        } catch (InterruptedException ex) {
            logger.log(Level.WARNING, "Caught {0}", ex);
            throw new RuntimeException(ex);
        }
    }

    private static class StartServer implements Runnable {

        private final IServer server;

        public StartServer(IServer server) {
            this.server = server;
        }

        @Override
        public void run() {
            this.server.start();
        }
    }
}
