package ass102.messnpet.webserver.server;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.response.IResponseWriter;

/**
 * Rozhraní pro objekty, které umí obsloužit HTTP požadavek a vrátit HTTP
 * odpověď.
 *
 * @author Petr Messner
 */
public interface IWebRequestHandler {

    /**
     * Zpracování HTTP požadavku
     *
     * Vrací instanci implementující rozhraní IResponseWriter - tzn. objekt,
     * který má metodu writeResponseToSocketOutputStream(OutputStream os),
     * která umí zapsat HTTP odpověď do output streamu TCP socketu.
     *
     * @param r HTTP požadavek
     * @return HTTP odpověď
     */
    public IResponseWriter processRequest(Request r);
}
