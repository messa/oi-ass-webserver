package ass102.messnpet.webserver.server;

/**
 * Factory pro snadné vytváření instancí HTTP serveru
 *
 * @author Petr Messner
 */
public class HTTPServerFactory {

    public static IServer newHTTPServer(int port, IWebRequestHandler requestHandler) {
        return new TCPServer(port, new SocketClientHandler(requestHandler));
    }

    public static IServer newThreadingHTTPServer(int port, IWebRequestHandler requestHandler) {
        ISocketClientHandler sch = new SocketClientHandler(requestHandler);
        SocketClientHandlerThreadPoolProxy p = new SocketClientHandlerThreadPoolProxy(sch);
        p.start();
        return new TCPServer(port, p);
    }

    public static IServer newHTTPServerInThread(int port, IWebRequestHandler requestHandler) {
        IServer httpServer = new TCPServer(port, new SocketClientHandler(requestHandler));
        return new ServerInThread(httpServer);
    }

    public static IServer startHTTPServerInThread(int port, IWebRequestHandler requestHandler) throws InterruptedException {
        TCPServer httpServer = new TCPServer(port, new SocketClientHandler(requestHandler));
        IServer serverInThread = new ServerInThread(httpServer);
        serverInThread.start();
        httpServer.waitUntilStarted();
        return serverInThread;
    }

    public static IServer newThreadingHTTPServerInThread(int port, IWebRequestHandler requestHandler) {
        ISocketClientHandler sch = new SocketClientHandler(requestHandler);
        SocketClientHandlerThreadPoolProxy p = new SocketClientHandlerThreadPoolProxy(sch);
        p.start();
        IServer httpServer = new TCPServer(port, p);
        return new ServerInThread(httpServer);
    }

    public static IServer startThreadingHTTPServerInThread(int port, IWebRequestHandler requestHandler) throws InterruptedException {
        ISocketClientHandler sch = new SocketClientHandler(requestHandler);
        SocketClientHandlerThreadPoolProxy p = new SocketClientHandlerThreadPoolProxy(sch);
        p.start();
        TCPServer httpServer = new TCPServer(port, p);
        IServer httpServerInThread = new ServerInThread(httpServer);
        httpServerInThread.start();
        httpServer.waitUntilStarted();
        return httpServerInThread;
    }

}
