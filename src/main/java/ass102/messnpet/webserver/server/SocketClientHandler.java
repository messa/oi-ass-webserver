package ass102.messnpet.webserver.server;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.request.RequestParser;
import ass102.messnpet.webserver.response.IResponseWriter;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * To, co zpracuje přijaté TCP spojení.
 *
 * Přečte HTTP požadavek (readRequest, RequestParser.parseRequest), předá ho
 * requestHandleru (requestHandler.processRequest), ten vrátí odpověď a ta je
 * zapsána zpět do soketu spojení (response.writeResponseToSocketOutputStream).
 *
 * @author Petr Messner
 */
public class SocketClientHandler implements ISocketClientHandler {

    private static final Logger logger = Logger.getLogger(SocketClientHandler.class.getName());
    private final IWebRequestHandler requestHandler;

    public SocketClientHandler(IWebRequestHandler requestHandler) {
        this.requestHandler = requestHandler;
    }

    @Override
    public void processRequest(Socket s) {
        try {
            InputStream is = s.getInputStream();
            BufferedReader isr = new BufferedReader(new InputStreamReader(is));
            BufferedOutputStream bos = new BufferedOutputStream(s.getOutputStream());

            String[] requestLines = readRequest(isr);

            Request request = RequestParser.parseRequest(requestLines);

            IResponseWriter response = requestHandler.processRequest(request);

            response.writeResponseToSocketOutputStream(bos);
            bos.flush();

            s.close();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Caught IOException", ex);
        }
    }

    private String[] readRequest(BufferedReader r) throws IOException {
        List<String> requestLines = new ArrayList<String>();
        for (;;) {
            String line = r.readLine();
            if (line.equals("")) {
                break;
            }
            requestLines.add(line);
        }
        return requestLines.toArray(new String[0]);
    }
}
