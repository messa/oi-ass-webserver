package ass102.messnpet.webserver.server;

import ass102.messnpet.webserver.pool.ThreadPool;
import java.net.Socket;

/**
 * @author Petr Messner
 */
public class SocketClientHandlerThreadPoolProxy implements ISocketClientHandler {

    ISocketClientHandler socketClientHandler;
    ThreadPool threadPool = new ThreadPool();

    public SocketClientHandlerThreadPoolProxy(ISocketClientHandler sch) {
        this.socketClientHandler = sch;
    }

    public void start() {
        this.threadPool.start();
    }

    @Override
    public void processRequest(final Socket s) {
        this.threadPool.process(new Runnable() {

            @Override
            public void run() {
                socketClientHandler.processRequest(s);
            }
        });
    }
}
