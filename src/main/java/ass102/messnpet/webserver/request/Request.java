package ass102.messnpet.webserver.request;

import java.util.HashMap;
import java.util.Map;

/**
 * Abstrakce HTTP požadavku.
 *
 * @author Petr Messner
 */
public class Request {

    private final String method;
    private final String path;
    private final String protocol;
    private final Map<String, String> headers;

    /**
     *
     * @param method HTTP metoda ("GET", "POST", ...)
     * @param path Cesta, včetně ? a query.
     * @param protocol HTTP protokol, "HTTP/1.0" nebo "HTTP/1.1"
     * @param headers Mapa s HTTP hlavičkami; klíče budou převedeny na lowercase
     */
    public Request(String method, String path, String protocol, Map<String, String> headers) {
        this.method = method == null ? null : method.toUpperCase();
        this.path = path;
        this.protocol = protocol;

        if (headers == null) {
            this.headers = null;
        } else {
            // hlavicky si pro jistotu zkopirujeme a ochranime pred zmenami
            Map<String, String> myHeaders = new HashMap<String, String>();
            for (Map.Entry<String, String> e : headers.entrySet()) {
                String key = e.getKey();
                String value = e.getValue();
                myHeaders.put(key.toLowerCase(), value);
            }
            this.headers = java.util.Collections.unmodifiableMap(myHeaders);
        }
    }

    public String getMethod() {
        return method;
    }

    public String getPath() {
        return this.path;
    }

    public String getProtocol() {
        return this.protocol;
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public String getHeader(String headerName) {
        if (this.headers == null) {
            return null;
        }
        return this.headers.get(headerName.toLowerCase());
    }

    /**
     * Pro ladící účely.
     */
    @Override
    public String toString() {
        return "<Request: " + getMethod() + " " + getPath() + ">";
    }
}
