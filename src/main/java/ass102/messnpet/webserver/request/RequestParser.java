package ass102.messnpet.webserver.request;

import ass102.messnpet.webserver.request.Request;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parsování HTTP požadavku
 *
 * @author Petr Messner
 */
public class RequestParser {

    private static Pattern firstLinePattern = Pattern.compile("^([A-Z]+) (/[^ ]*) (HTTP/1\\.[01])$");
    private static Pattern headerPattern = Pattern.compile("^([^ ]+): (.*)");

    /**
     * Zpracuje HTTP hlavičku a vrátí Request.
     *
     * @param lines HTTP hlavička (po řádcích)
     * @return Request s atributy vyplněnými podle dat v hlavičce
     */
    public static Request parseRequest(List<String> lines) {
        return parseRequest(lines.toArray(new String[0]));
    }

    /**
     * Zpracuje HTTP hlavičku a vrátí Request.
     *
     * @param lines HTTP hlavička (po řádcích)
     * @return Request s atributy vyplněnými podle dat v hlavičce
     */
    public static Request parseRequest(String[] lines) {
        if (lines.length == 0) {
            throw new RequestParserException("Status line missing");
        }

        String statusLine = lines[0];
        Matcher m = firstLinePattern.matcher(statusLine);
        if (!m.matches()) {
            throw new RequestParserException("Invalid status line: " + statusLine);
        }

        String method = m.group(1);
        String path = m.group(2);
        String protocol = m.group(3);

        Map<String, String> headers = new TreeMap<String, String>();

        for (int i = 1; i < lines.length; i++) {
            String line = lines[i];

            Matcher hm = headerPattern.matcher(line);
            if (!hm.matches()) {
                throw new RequestParserException("Invalid header line: " + line);
            }
            String headerName = hm.group(1);
            String headerValue = hm.group(2);
            headers.put(headerName.toLowerCase(), headerValue);
        }

        return new Request(method, path, protocol, headers);
    }
}
