package ass102.messnpet.webserver.request;

/**
 * Výjimka označující nějakou chybu vzniklou při parsování HTTP hlavičky
 * třídou RequestParser
 *
 * @author Petr Messner
 */
public class RequestParserException extends RuntimeException {

    public RequestParserException(String string) {
        super(string);
    }

    public RequestParserException(Throwable thrwbl) {
        super(thrwbl);
    }

    public RequestParserException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }
}
