package ass102.messnpet.webserver.pool;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread pool
 *
 * @author Petr Messner
 */
public class ThreadPool {

    private static final Logger logger = Logger.getLogger(ThreadPool.class.getName());
    private int maxWorkerCount = 150;
    private int maxWorkerWaitingTimeMSec = 1000;
    private final BlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
    private final ThreadGroup threadGroup = new ThreadGroup("ThreadPool");
    private final Set<Worker> workers = new HashSet<Worker>();
    private int waitingThreads = 0;
    private boolean started = false;

    /**
     * Konstruktor.
     *
     * Pouze nastaví výchozí konfigurační hodnoty; workery se spouštějí až podle
     * potřeby.
     */
    public ThreadPool() {
    }

    public int getMaxWorkerCount() {
        return maxWorkerCount;
    }

    public ThreadPool setMaxWorkerCount(int c) {
        this.maxWorkerCount = c;
        return this;
    }

    public int getMaxWorkerWaitingTimeMSec() {
        return maxWorkerWaitingTimeMSec;
    }

    public void start() {
        // Tato metoda sice nic nedělá, ale pro jistotu, kdyby v budoucnu
        // něco dělat měla, chceme, aby se tato metoda musela volat před
        // samotným použitím ThreadPoolu.
        this.started = true;
    }

    /**
     * Přidá do fronty.
     *
     * @param runnable
     */
    public void process(Runnable runnable) {
        if (!started) {
            throw new RuntimeException("not started");
        }
        logger.log(Level.INFO, "Processing {0}", runnable);
        synchronized (this) {
            if (waitingThreads == 0 && workers.size() < maxWorkerCount) {
                logger.log(Level.INFO, "Starting new worker for processing {0}", runnable);
                startNewWorker();
            }
            this.queue.add(runnable);
        }
    }

    private void startNewWorker() {
        Worker w = new Worker();
        logger.log(Level.INFO, "Starting new worker thread: {0}", w);
        synchronized (this) {
            this.workers.add(w);
            w.start();
        }
    }

    public void stop() throws InterruptedException {
        Worker[] ws;

        synchronized (this) {
            ws = this.workers.toArray(new Worker[0]);
            for (Worker w : ws) {
                w.stop();
            }
        }

        for (Worker w : ws) {
            logger.log(Level.INFO, "Joining {0}", w);
            w.join();
            logger.log(Level.INFO, "Joined {0}", w);
        }
    }

    private void workerTerminated(Worker worker) {
        logger.log(Level.INFO, "Worker thread terminated: {0}", worker);
        synchronized (this) {
            this.workers.remove(worker);
        }
    }

    private void incrementWaitingThreads() {
        synchronized (this) {
            this.waitingThreads++;
        }
    }

    private void decrementWaitingThreads() {
        synchronized (this) {
            this.waitingThreads--;
            if (this.waitingThreads < 0) {
                // toto by se opravdu stavat nemelo
                throw new RuntimeException("waitingThreads < 0");
            }
        }
    }

    class Worker implements Runnable {

        Thread t;

        public Worker() {
            this.t = new Thread(threadGroup, this, "worker");
        }

        private void start() {
            this.t.start();
        }

        @Override
        public void run() {
            while (true) {
                Runnable r;
                try {
                    incrementWaitingThreads();
                    try {
                        r = queue.poll(maxWorkerWaitingTimeMSec, TimeUnit.MILLISECONDS);
                    } finally {
                        decrementWaitingThreads();
                    }

                } catch (InterruptedException ex) {
                    break;
                }

                if (r == null) {
                    break;
                }

                r.run();
            }

            // ukončení vlákna
            workerTerminated(this);
        }

        private void stop() {
            this.t.interrupt();
        }

        private void join() throws InterruptedException {
            while (this.t.isAlive()) {
                this.t.join(1000);
            }
        }
    }
}
