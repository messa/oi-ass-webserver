package ass102.messnpet.webserver;

import ass102.messnpet.webserver.handlers.AuthRequestHandler;
import ass102.messnpet.webserver.handlers.CachingWebRequestHandler;
import ass102.messnpet.webserver.handlers.FileServingWebRequestHandler;
import ass102.messnpet.webserver.handlers.HelloWorldRequestHandler;
import ass102.messnpet.webserver.server.HTTPServerFactory;
import ass102.messnpet.webserver.server.IServer;
import ass102.messnpet.webserver.server.IWebRequestHandler;
import ass102.messnpet.webserver.util.TCPUtils;
import java.io.File;

/**
 *
 * @author Petr Messner
 */
public class Main {

    /**
     * Vstupní bod programu.
     *
     * Na základě argumentů příkazové řádky spustí webserver na daném portu nebo
     * provede benchmark.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Configuration conf = new Configuration();
        parseArgs(args, conf);

        switch (conf.getAction()) {
            case SERVE:
                serve(conf);
                break;
            case RUN_BENCHMARK:
                runBenchmark(conf);
                break;
        }
    }

    /**
     * Výjimka pro neznámý parametr příkazové řádky
     */
    public static class UnknownArgumentException extends RuntimeException {

        public UnknownArgumentException(String string) {
            super(string);
        }
    }

    /**
     * Zpracování argumentů příkazové řádky do konfigurace
     *
     * @param args Argumenty příkazové řádky
     * @param conf Konfigurační objekt, který bude změněn na základě argumentů
     */
    public static void parseArgs(String args[], Configuration conf) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("--benchmark")) {
                conf.setAction(Configuration.Action.RUN_BENCHMARK);
                continue;
            }
            if (args[i].equals("--port") || args[i].equals("-p")) {
                conf.setPort(Integer.valueOf(args[i + 1]));
                i++;
                continue;
            }
            throw new UnknownArgumentException("Unknown argument " + args[i]);
        }
    }

    private static void serve(Configuration conf) {
        // Request handler zpracovává HTTP požadavek a vrací HTTP odpověď.
        // Tento request handler sdílí daný adresář přes HTTP.
        IWebRequestHandler requestHandler = new FileServingWebRequestHandler(new File("."));

        // Request handler obalíme handlerem, který kešuje HTTP odpovědi.
        requestHandler = new CachingWebRequestHandler(requestHandler);

        // To celé obalíme handlerem, který požaduje HTTP Basic autentikaci
        requestHandler = new AuthRequestHandler(requestHandler, "aaa/aaa").add("aaa", "aaa");

        // Vytvoříme HTTP server
        IServer server = HTTPServerFactory.newThreadingHTTPServer(conf.getPort(), requestHandler);

        // Spuštění HTTP serveru, v tomto vlákně - metoda start() poběží do
        // konce programu
        server.start();
    }

    private static void runBenchmark(Configuration cobf) {
        int port = TCPUtils.getUnunsedPort();

        IWebRequestHandler requestHandler = new HelloWorldRequestHandler();
        IServer server = HTTPServerFactory.newThreadingHTTPServerInThread(port, requestHandler);
        server.start();

        System.out.println("Started server at port " + port);

        Benchmark b = new Benchmark(port);
        b.run(100, 100);

        System.out.println("Stopping server...");
        server.stop();

        b.printSummary(System.out);
    }

    /**
     * Konfigurace
     */
    public static class Configuration {

        public enum Action {

            SERVE, RUN_BENCHMARK
        }
        private int port = 8000;
        private Action action = Action.SERVE;

        public Configuration() {
        }

        public void setAction(Action action) {
            this.action = action;
        }

        public Action getAction() {
            return action;
        }

        public void setPort(int port) {
            this.port = port;
        }

        public int getPort() {
            return port;
        }
    }
}
