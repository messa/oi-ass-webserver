package ass102.messnpet.webserver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Zátěžový test HTTP serveru
 *
 * @author Petr Messner
 */
public class Benchmark {

    int port;
    int successful = 0;
    int failures = 0;
    long startTime = 0;
    long endTime = 0;

    public Benchmark(int port) {
        this.port = port;
    }

    /**
     * Runnable sloužící ke spuštění metody Benchmark.run
     */
    private static class RunRunner implements Runnable {

        Benchmark b;
        int count;

        public RunRunner(Benchmark b, int count) {
            this.b = b;
            this.count = count;
        }

        public void run() {
            this.b.run(this.count);
        }
    }

    /**
     * Spuštění benchmarku ve více vláknech
     *
     * @param count Počet požadavků v každém vlákně
     * @param threadCount Počet vláken
     */
    void run(final int count, int threadCount) {
        System.out.println(
                "Running " + count + " requests in "
                + threadCount + " threads each...");
        Thread threads[] = new Thread[threadCount];
        for (int i = 0; i < threadCount; i++) {
            threads[i] = new Thread(new RunRunner(this, count));
            threads[i].start();
        }
        for (int i = 0; i < threadCount; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    /**
     * Spuštění benchmarku
     *
     * Metoda je připravena pro běh ve více vláknech; pro spuštění ve více
     * vláknech slouží metoda run se dvěma parametry.
     *
     * @param count Počet požadavků
     */
    void run(int count) {
        System.out.println("Running " + count + " requests...");

        synchronized (this) {
            if (this.startTime == 0) {
                this.startTime = System.currentTimeMillis();
            }
        }

        for (int i = 0; i < count; i++) {

            try {
                System.out.println(i + "/" + count);

                Socket s = new Socket("127.0.0.1", this.port);
                OutputStreamWriter w = new OutputStreamWriter(s.getOutputStream());
                w.write("GET / HTTP/1.0\r\n");
                w.write("Host: 127.0.0.1\r\n");
                w.write("\r\n");
                w.flush();

                BufferedReader r = new BufferedReader(new InputStreamReader(s.getInputStream()));
                String line;
                line = r.readLine();
                if (!line.equals("HTTP/1.0 200 OK")) {
                    throw new Exception("Invalid first line of response: " + line);
                }
                /*
                 * while (! line.equals("")) { line = r.readLine(); }
                 */
                r.close();

                synchronized (this) {
                    successful++;
                }

            } catch (Exception e) {
                System.err.println("Caught " + e);
                synchronized (this) {
                    failures++;
                }
            }

        }

        synchronized (this) {
            this.endTime = System.currentTimeMillis();
        }
    }

    /**
     * Výpis shrnutí testu - počet úspěšných odpovědí, doba trvání
     *
     * @param out Výstupní stream, např. System.out
     */
    void printSummary(PrintStream out) {
        out.println(
                "===== " + this.successful + " successful requests, "
                + this.failures + " failures in "
                + (this.endTime - this.startTime) / 1000.0 + " s =====");
    }
}
