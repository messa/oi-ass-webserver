package ass102.messnpet.webserver.response;

/**
 * HTTP odpověď oznamující chybu na straně serveru
 *
 * @author Petr Messner
 */
public class ServerErrorResponse extends Response {

    @Override
    public int getStatusCode() {
        return 500;
    }

    @Override
    public String getStatusMessage() {
        return "Server Error";
    }
}
