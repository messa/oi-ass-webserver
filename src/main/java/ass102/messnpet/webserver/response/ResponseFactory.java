package ass102.messnpet.webserver.response;

/**
 * Factory pro HTTP odpovědi
 *
 * @author Petr Messner
 */
public class ResponseFactory {

    public static Response createOKResponse() {
        return new OKResponse();
    }

    public static Response createNotFoundResponse() {
        return new NotFoundResponse();
    }

    public static Response createServerErrorResponse() {
        return new ServerErrorResponse();
    }

    public static Response createAuthorizationRequiredResponse(String realm) {
        return new AuthorizationRequiredResponse(realm);
    }
}
