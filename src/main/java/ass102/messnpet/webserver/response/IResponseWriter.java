package ass102.messnpet.webserver.response;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Rozhraní pro objekty, které umí zapsat HTTP odpověď do streamu socketu.
 *
 * @author Petr Messner
 */
public interface IResponseWriter {

    /**
     * Zapsání odpovědi do soketu klientovi
     *
     * @param os OutputStream patřící k Socketu
     * @throws IOException
     */
    public void writeResponseToSocketOutputStream(OutputStream os) throws IOException;
}
