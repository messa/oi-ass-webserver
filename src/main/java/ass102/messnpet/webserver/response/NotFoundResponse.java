package ass102.messnpet.webserver.response;

/**
 * HTTP odpověď oznamující, že požadovaná stránka nebyla nalezena
 *
 * @author Petr Messner
 */
public class NotFoundResponse extends Response {

    @Override
    public int getStatusCode() {
        return 404;
    }

    @Override
    public String getStatusMessage() {
        return "Not Found";
    }
}
