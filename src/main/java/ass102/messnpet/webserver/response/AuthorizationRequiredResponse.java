package ass102.messnpet.webserver.response;

/**
 * @author Petr Messner
 */
class AuthorizationRequiredResponse extends Response {

    public AuthorizationRequiredResponse(String realm) {
        setHeader("WWW-Authenticate", "Basic realm=\"" + realm + "\"");
    }

    @Override
    public int getStatusCode() {
        return 401;
    }

    @Override
    public String getStatusMessage() {
        return "Authorization Required";
    }
}
