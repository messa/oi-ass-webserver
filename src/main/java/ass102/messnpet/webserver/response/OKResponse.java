package ass102.messnpet.webserver.response;

/**
 * HTTP odpověď
 *
 * @author Petr Messner
 */
public class OKResponse extends Response {

    @Override
    public int getStatusCode() {
        return 200;
    }

    @Override
    public String getStatusMessage() {
        return "OK";
    }
}
