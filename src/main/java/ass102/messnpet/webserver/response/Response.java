package ass102.messnpet.webserver.response;

import ass102.messnpet.webserver.util.IOUtils;
import java.io.*;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstrankce HTTP odpovědi
 *
 * @author Petr Messner
 */
public abstract class Response implements IResponseWriter {

    private static final Charset utf8 = Charset.forName("utf-8");
    private InputStream body;
    private Map<String, String> headers = new HashMap<String, String>();
    private boolean written = false;

    public abstract int getStatusCode();

    public abstract String getStatusMessage();

    public Response() {
    }

    public void setHeader(String name, String value) {
        this.headers.put(name, value);
    }

    /**
     *
     * @param body Tělo HTTP odpovědi
     * @return reference na sebe sama
     */
    public Response setBody(String body) {
        byte[] bodyBytes = body.getBytes(utf8);
        this.body = new ByteArrayInputStream(bodyBytes);
        return this;
    }

    /**
     *
     * @param body Tělo HTTP odpovědi
     * @return reference na sebe sama
     */
    public Response setBody(byte[] body) {
        this.body = new ByteArrayInputStream(body);
        return this;
    }

    /**
     *
     * @param body Tělo HTTP odpovědi
     * @return reference na sebe sama
     */
    public Response setBody(InputStream body) {
        this.body = body;
        return this;
    }

    /**
     * Zapsání odpovědi do soketu klientovi
     *
     * @param bos OutputStream patřící k Socketu
     * @throws IOException
     */
    @Override
    public void writeResponseToSocketOutputStream(OutputStream os) throws IOException {
        // XXX TODO pridelat hlavicky

        assert !this.written;
        this.written = true;

        {
            Writer w = new OutputStreamWriter(os, "UTF-8");
            // první řádek - statový kód
            w.write(String.format("HTTP/1.0 %d %s\r\n", this.getStatusCode(), this.getStatusMessage()));

            // Hlavičky
            for (Map.Entry<String, String> e : this.headers.entrySet()) {
                w.write(e.getKey() + ": " + e.getValue() + "\r\n");
            }

            // prázdný řádek oddělující HTTP hlavičky od obsahu
            w.write("\r\n");
            w.flush();
        }

        // obsah
        if (this.body != null) {
            IOUtils.copy(this.body, os);
        }
    }
}
