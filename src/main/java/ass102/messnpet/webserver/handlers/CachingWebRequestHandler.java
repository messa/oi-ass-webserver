package ass102.messnpet.webserver.handlers;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.response.IResponseWriter;
import ass102.messnpet.webserver.server.IWebRequestHandler;
import ass102.messnpet.webserver.util.cache.ICache;
import ass102.messnpet.webserver.util.cache.SoftReferenceCache;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Web request handler, který kešuje výsledky jiného handleru
 *
 * @author Petr Messner
 */
public class CachingWebRequestHandler implements IWebRequestHandler {

    private static final Logger logger = Logger.getLogger(CachingWebRequestHandler.class.getName());

    private static class ResponseWriter implements IResponseWriter {

        private final byte[] data;

        public ResponseWriter(byte[] data) {
            this.data = data;
        }

        @Override
        public void writeResponseToSocketOutputStream(OutputStream bos) throws IOException {
            bos.write(this.data);
        }
    }
    private IWebRequestHandler handler;
    private ICache<String, byte[]> cache = new SoftReferenceCache<String, byte[]>();
    private int maxCachedResponseSizeBytes = 10 * 1024 * 1024;

    public CachingWebRequestHandler(IWebRequestHandler handler) {
        this.handler = handler;
        this.cache.run();
    }

    @Override
    protected void finalize() throws Throwable {
        this.cache.stop();
        super.finalize();
    }

    public int getMaxCachedResponseSizeBytes() {
        return maxCachedResponseSizeBytes;
    }

    public void setMaxCachedResponseSizeBytes(int maxCachedResponseSizeBytes) {
        this.maxCachedResponseSizeBytes = maxCachedResponseSizeBytes;
    }

    @Override
    public IResponseWriter processRequest(Request r) {
        String cacheKey = r.getPath();
        byte[] cachedValue = this.cache.get(cacheKey);
        if (cachedValue == null) {
            logger.log(Level.INFO, "Key {0} not found in cache", cacheKey);

            IResponseWriter resp = this.handler.processRequest(r);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                resp.writeResponseToSocketOutputStream(baos);
            } catch (IOException ex) {
                Logger.getLogger(CachingWebRequestHandler.class.getName()).log(Level.SEVERE, null, ex);
                throw new RuntimeException(ex);
            }

            cachedValue = baos.toByteArray();
            this.cache.set(cacheKey, cachedValue);
            logger.log(Level.INFO, "Key {0} was added to cache with {1} bytes of data", new Object[]{cacheKey, cachedValue.length});
        } else {
            logger.log(Level.INFO, "Key {0} found in cache with {1} bytes of data", new Object[]{cacheKey, cachedValue.length});

        }
        //return ResponseFactory.createOKResponse().setBody(cachedValue);
        return new ResponseWriter(cachedValue);

    }
}
