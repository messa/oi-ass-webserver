package ass102.messnpet.webserver.handlers;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.response.IResponseWriter;
import ass102.messnpet.webserver.response.ResponseFactory;
import ass102.messnpet.webserver.server.IWebRequestHandler;

/**
 * Request handler, který vrací stránku s textem "Hello, World!"
 *
 * @author Petr Messner
 */
public class HelloWorldRequestHandler implements IWebRequestHandler {

    @Override
    public IResponseWriter processRequest(Request r) {
        return ResponseFactory.createOKResponse().setBody("Hello, World!");
    }
}
