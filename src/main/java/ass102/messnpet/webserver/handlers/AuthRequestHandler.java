package ass102.messnpet.webserver.handlers;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.response.IResponseWriter;
import ass102.messnpet.webserver.response.ResponseFactory;
import ass102.messnpet.webserver.server.IWebRequestHandler;
import ass102.messnpet.webserver.util.Base64Utils;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Request handler, který obalí jiný request handler a poskytne jednoduché
 * ověření přístupu (HTTP Basic access authentication).
 *
 * @author Petr Messner
 */
public class AuthRequestHandler implements IWebRequestHandler {

    private static final Logger logger = Logger.getLogger(AuthRequestHandler.class.getName());
    private IWebRequestHandler handler;
    private String realm;
    private Map<String, String> passwords = new HashMap<String, String>();

    public AuthRequestHandler(IWebRequestHandler handler, String realm) {
        this.handler = handler;
        this.realm = realm;
    }

    public IResponseWriter processRequest(Request r) {
        String header = r.getHeader("Authorization");
        if (header != null && header.startsWith("Basic ")) {
            String base64Stuff = header.substring(6);
            String decodedStuff = Base64Utils.decode(base64Stuff);
            int i = decodedStuff.indexOf(":");
            if (i != 1) {
                String name = decodedStuff.substring(0, i);
                String password = decodedStuff.substring(i + 1);
                String correctPassword = this.passwords.get(name);
                if (correctPassword != null && correctPassword.equals(password)) {
                    // vsechno OK
                    return handler.processRequest(r);
                }
            }
        }
        return ResponseFactory.createAuthorizationRequiredResponse(this.realm);
    }

    /**
     * Přidá jméno a heslo do seznamu povolených jmen a hesel.
     *
     * @param name Jméno
     * @param password Heslo
     * @return Vrací instanci AuthRequestHandleru pro snadnější použití
     */
    public AuthRequestHandler add(String name, String password) {
        this.passwords.put(name, password);
        return this;
    }
}
