package ass102.messnpet.webserver.handlers;

import ass102.messnpet.webserver.request.Request;
import ass102.messnpet.webserver.response.IResponseWriter;
import ass102.messnpet.webserver.response.ResponseFactory;
import ass102.messnpet.webserver.server.IWebRequestHandler;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Web request handler zprostředkovávající adresář na disku
 *
 * @author Petr Messner
 */
public class FileServingWebRequestHandler implements IWebRequestHandler {

    public static class DocumentRootIsNotDirectoryException extends RuntimeException {

        public DocumentRootIsNotDirectoryException(File documentRoot) {
            super("Document root " + documentRoot + "is not a directory");
        }
    }
    private File documentRoot;

    public FileServingWebRequestHandler(File documentRoot) {
        if (!documentRoot.isDirectory()) {
            throw new DocumentRootIsNotDirectoryException(documentRoot);
        }
        this.documentRoot = documentRoot.getAbsoluteFile();
        assert this.documentRoot.isDirectory();
    }

    @Override
    public IResponseWriter processRequest(Request r) {
        String pathStr = r.getPath();
        File fullPath = new File(this.documentRoot, pathStr);
        fullPath = fullPath.getAbsoluteFile();

        if (fullPath.isDirectory()) {
            return directoryListing(fullPath);
        }

        if (fullPath.isFile()) {
            try {
                return fileContents(fullPath);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FileServingWebRequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ResponseFactory.createNotFoundResponse();
    }

    private IResponseWriter directoryListing(File fullPath) {
        StringBuilder b = new StringBuilder();

        for (File f : fullPath.listFiles()) {
            b.append("<a href=\"");
            b.append(f.getName());
            b.append("\">");
            b.append(f.getName());
            b.append("</a>");
            b.append("<br>\r\n");
        }

        return ResponseFactory.createOKResponse().setBody(b.toString());
    }

    private IResponseWriter fileContents(File fullPath) throws FileNotFoundException {
        byte[] byteArray = null;

        FileInputStream fis = new FileInputStream(fullPath);
        BufferedInputStream bis = new BufferedInputStream(fis);

        long fileSizeLong = fullPath.length();
        if (fileSizeLong > 100 * 1048576) {
            // XXX TODO prepracovat tak, aby to fungovalo s libovolne velkymi soubory
            throw new RuntimeException("File is too large");
        }
        int fileSize = (int) fileSizeLong;

        byteArray = new byte[fileSize];
        int bytesRead;
        try {
            bytesRead = bis.read(byteArray, 0, fileSize);
        } catch (IOException ex) {
            Logger.getLogger(FileServingWebRequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseFactory.createServerErrorResponse();
        }
        if (bytesRead != fileSize) {
            throw new RuntimeException("bytesRead != fileSize");
        }
        assert byteArray != null;
        return ResponseFactory.createOKResponse().setBody(byteArray);
    }
}
