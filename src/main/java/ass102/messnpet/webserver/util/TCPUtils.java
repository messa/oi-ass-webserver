package ass102.messnpet.webserver.util;

import java.io.IOException;
import java.net.ServerSocket;

/**
 *
 * @author Petr Messner
 */
public class TCPUtils {


    /**
     * Vrátí číslo TCP portu, které není obsazeno.
     *
     * Užitečné např. pro testování. Je to použito v unit testech.
     */
    public static int getUnunsedPort() {
        try {
            // A port of 0 creates a socket on any free port.
            ServerSocket s = new ServerSocket(0);
            int port = s.getLocalPort();
            // povolíme reuse, aby se ten soket nezablokoval
            s.setReuseAddress(true);
            s.close();
            return port;
        } catch (IOException ex) {
            throw new RuntimeException("Could not get free port number", ex);
        }
    }
}
