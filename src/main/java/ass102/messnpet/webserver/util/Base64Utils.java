package ass102.messnpet.webserver.util;

import java.nio.charset.Charset;
import org.apache.commons.codec.binary.Base64;

/**
 * Pomocné funkce pro práci s Base64 kódováním
 *
 * Tato třída existuje proto, aby v případě změny implementace Base64 kódování
 * stačilo upravit program na tomto jediném místě.
 *
 * @author Petr Messner
 */
public class Base64Utils {

    public static String decode(String base64Stuff) {
        byte[] decoded = new Base64().decode(base64Stuff);
        return new String(decoded, Charset.forName("UTF-8"));
    }
}
