package ass102.messnpet.webserver.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Pomocné funkce týkající se vstupu a výstupu.
 *
 * @author Petr Messner
 */
public class IOUtils {

    /**
     * Zkopíruje všechna data z in do out.
     *
     * Použije výchozí velikost bloku.
     *
     * @param in
     * @param out
     * @throws IOException
     */
    public static void copy(InputStream in, OutputStream out) throws IOException {
        copy(in, out, 65536);
    }

    /**
     * Zkopíruje všechna data z in do out.
     *
     * @param in
     * @param out
     * @param bufferSize Velikost bloku, po kterých se kopíruje
     * @throws IOException
     */
    public static void copy(InputStream in, OutputStream out, int bufferSize) throws IOException {
        byte[] buffer = new byte[bufferSize];

        while (true) {
            int bytesRead = in.read(buffer);
            if (bytesRead == -1) {
                break;
            }
            out.write(buffer, 0, bytesRead);
        }
    }
}
