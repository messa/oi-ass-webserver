package ass102.messnpet.webserver.util.cache;

/**
 * Obecné rozhraní pro cache.
 *
 * @author Petr Messner
 */
public interface ICache<TKEY, TVALUE> {

    /**
     * Získání hodnoty z cache.
     *
     * @param key Klíč cache
     * @return Kešované hodnota nebo null, pokud klíč nebyl nalezen
     */
    public TVALUE get(TKEY key);

    /**
     * Uložení hodnoty do keše pod daným klíčem.
     *
     * @param key Klíč
     * @param value Kešovaná hodnota
     */
    public void set(TKEY key, TVALUE value);

    public void run();

    public void stop();
}
