package ass102.messnpet.webserver.util.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Jednoduchá keš.
 *
 * @author Petr Messner
 */
public class SimpleCache<TKEY, TVALUE> implements ICache<TKEY, TVALUE> {

    /**
     * Struktura pro uložení jako hodnota mapy cache
     *
     * @param <TDATA> Typ dat, který se kešuje
     */
    private class MapValue<TDATA> {

        TDATA data;
        int lastUsed;

        public MapValue(TDATA data, int n) {
            this.data = data;
            this.lastUsed = n;
        }
    }
    private final Map<TKEY, MapValue<TVALUE>> map = new HashMap<TKEY, MapValue<TVALUE>>();
    private int counter = 0;
    private int maxItemCount = 100;
    private int cleanItemCount = 150;

    public SimpleCache() {
    }

    @Override
    public void run() {
    }

    @Override
    public void stop() {
    }

    public void setMaxItemCount(int maxItemCount) {
        this.maxItemCount = maxItemCount;
    }

    public int getMaxItemCount() {
        return maxItemCount;
    }

    public void setCleanItemCount(int cleanItemCount) {
        this.cleanItemCount = cleanItemCount;
    }

    public int getCleanItemCount() {
        return cleanItemCount;
    }

    /**
     * Získání hodnoty z cache.
     *
     * @param key Klíč cache
     * @return Kešované hodnota nebo null, pokud klíč nebyl nalezen
     */
    @Override
    public synchronized TVALUE get(TKEY key) {
        MapValue<TVALUE> mk = this.map.get(key);
        if (mk == null) {
            return null;
        }
        this.counter += 1;
        mk.lastUsed = this.counter;
        return mk.data;
    }

    /**
     * Uložení hodnoty do keše pod daným klíčem.
     *
     * @param key Klíč
     * @param value Kešovaná hodnota
     */
    @Override
    public synchronized void set(TKEY key, TVALUE value) {
        clean();
        this.counter += 1;
        MapValue<TVALUE> mk = new MapValue<TVALUE>(value, this.counter);
        this.map.put(key, mk);
    }

    private static class KeyWithPriority<T> implements Comparable<KeyWithPriority<T>> {

        T key;
        int prio;

        public KeyWithPriority(T key, int prio) {
            this.key = key;
            this.prio = prio;
        }

        @Override
        public int compareTo(KeyWithPriority<T> other) {
            if (this.prio < other.prio) {
                return -1;
            }
            if (this.prio > other.prio) {
                return 1;
            }
            return 0;
        }
    }

    public synchronized void clean() {
        assert this.maxItemCount <= this.cleanItemCount;
        if (this.map.size() < this.cleanItemCount) {
            return;
        }
        PriorityQueue<KeyWithPriority<TKEY>> pq = new PriorityQueue<KeyWithPriority<TKEY>>(this.map.size());
        for (Map.Entry<TKEY, MapValue<TVALUE>> e : this.map.entrySet()) {
            pq.add(new KeyWithPriority<TKEY>(e.getKey(), e.getValue().lastUsed));
        }
        while (this.map.size() > this.maxItemCount) {
            KeyWithPriority<TKEY> kwp = pq.poll();
            this.map.remove(kwp.key);
        }
    }
}
