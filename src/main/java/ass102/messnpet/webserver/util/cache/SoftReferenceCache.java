package ass102.messnpet.webserver.util.cache;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Keš implementovaná s využitím SoftReference.
 *
 * @author Petr Messner
 */
public class SoftReferenceCache<TKEY, TVALUE> implements ICache<TKEY, TVALUE> {

    private Map<TKEY, SoftReference<TVALUE>> map = new HashMap<TKEY, SoftReference<TVALUE>>();
    private Thread cleanupThread = null;

    @Override
    public void run() {
        this.runCleanupThread();
    }

    @Override
    public void stop() {
        try {
            this.stopCleanupThread();
        } catch (InterruptedException ex) {
            Logger.getLogger(SoftReferenceCache.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Získání hodnoty z cache.
     *
     * @param key Klíč cache
     * @return Kešované hodnota nebo null, pokud klíč nebyl nalezen
     */
    @Override
    public synchronized TVALUE get(TKEY key) {
        SoftReference<TVALUE> sr = this.map.get(key);
        if (sr == null) {
            return null;
        }
        TVALUE v = sr.get();
        if (v == null) {
            this.map.remove(key);
        }
        return v;
    }

    /**
     * Uložení hodnoty do keše pod daným klíčem.
     *
     * @param key Klíč
     * @param value Kešovaná hodnota
     */
    @Override
    public void set(TKEY key, TVALUE value) {
        this.set(key, new SoftReference<TVALUE>(value));
    }

    protected synchronized void set(TKEY key, SoftReference<TVALUE> valueSR) {
        this.map.put(key, valueSR);
    }

    private class CleanupRunnable implements Runnable {

        private int sleepTimeSec = 10;

        @Override
        public void run() {
            while (!Thread.interrupted()) {
                cleanup();
                try {
                    Thread.sleep(sleepTimeSec * 1000);
                } catch (InterruptedException ex) {
                    return;
                }
            }
        }
    }

    /**
     * Spuštění vlákna, které provádí úklid (cleanup).
     */
    public synchronized void runCleanupThread() {
        assert this.cleanupThread == null;
        this.cleanupThread = new Thread(new CleanupRunnable(), "softrefcache-cleanup");
        this.cleanupThread.start();
    }

    /**
     * Ukončení vlákna, které provádí úklid (cleanup).
     */
    public synchronized void stopCleanupThread() throws InterruptedException {
        assert this.cleanupThread != null;
        while (this.cleanupThread.isAlive()) {
            this.cleanupThread.interrupt();
            this.cleanupThread.join(10);
        }
        this.cleanupThread = null;
    }

    /**
     * Provedení úklidu keše.
     */
    public synchronized void cleanup() {
        Map<TKEY, SoftReference<TVALUE>> newMap = new HashMap<TKEY, SoftReference<TVALUE>>();
        for (Map.Entry<TKEY, SoftReference<TVALUE>> e : this.map.entrySet()) {
            if (e.getValue().get() != null) {
                newMap.put(e.getKey(), e.getValue());
            }
        }
        this.map = newMap;
    }

    /**
     * Pro účely testování.
     *
     * @return Počet uložených softreferencí.
     */
    protected synchronized int getMapSize() {
        return this.map.size();
    }
}
