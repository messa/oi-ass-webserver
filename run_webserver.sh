#!/bin/bash

set -ex

mvn clean
mvn package

java -jar target/Webserver-*.jar

