
# Webserver

Semestrální práce pro předmět Architektury softwarových systémů v letním
semestru 2011/2012.

## Zadání

Naimplementujste jednoduchý více vláknový webový server

Požadavky na Vaše řešení:

 - schopnost přijmutí velkého počtu požadavků najednou (~100 simultálních požadavků)
 - efektivní využívání více vláken
 - cache
 - implementace zpracování GET hlavičky
 - implementace BASIC autentifikace (info [zde](http://cs.wikipedia.org/wiki/Basic_access_authentication))
 - unit testy jednotlivých modulů (JUnit), stress testy celého systému

